﻿using System;

namespace MonAppliConsole
{
class MethodesCalcul
{
    public void InfosObjet()
    {
        Console.WriteLine("Appel depuis l'instance d'un objet.");
    }

    /// <summary>
    /// Calcule la somme de 2 entiers
    /// </summary>
    /// <param name="a">Entier n°1</param>
    /// <param name="b">Entier n°2</param>
    /// <param name="min">Argument en sortie qui contient la valeur minimale entre a et b (entier)</param>
    /// <returns>Une somme de a et de b</returns>
    public static int Somme(int a, int b, out int min)
    {
        min = a;
        return a + b;
    }

    public static void Somme2(int a, int b, out int res)
    {
        res = a + b;
    }

    public static void Transform(ref int x)
    {
        System.Console.WriteLine(x);
        x += 5;
    }

    public static string GetNom()
    {
        System.Console.Write("Entrez votre nom : ");
        return System.Console.ReadLine();
    }

    public static int f(int x)
    {
        if (x == 0)
            throw new ArgumentException("x ne doit pas être égal à 0");

        var y = x *= 45;

        if (y == 45)
            throw new ApplicationException("La valeur de retour n'est pas correcte (45)");

        return y;
    }

    public static void Enregistrer(out int nbElementsEnregistres, params Program.mDate[] dates)
    {
        nbElementsEnregistres = 0;

        foreach (var date in dates)
        {
            var enregistrementOk = false;
            //Enregistrement en base de données
            enregistrementOk = true; // Dans la vraie vie, c'est une fonction qui retourne le résultat

            if (enregistrementOk)
            {
                ++nbElementsEnregistres;
            }
        }
    }

    // Paramètres optionnels
    public static int Somme3(int a = 0, int b = 0, int c = 10, int d = 0, int e = 0)
    {
        return a + b + c + d + e;
    }

    // Surcharge
    public static int Somme4(int a, int b)
    {
        return a + b;
    }

    public static int Somme4(int a, int b, int c)
    {
        return Somme4(a, b) + c;
    }

    public static int Somme4(int a, int b, int c, int d)
    {
        return Somme4(a, b, c) + d;
    }

    public static int Somme4(string a, string b)
    {
        var aEntier = System.Convert.ToInt32(a);
        var bEntier = System.Convert.ToInt32(b);
        return Somme4(aEntier, bEntier);
    }

    //Exceptions
    public static int MaMethodeQuiAppelleF(int x)
    {
        try
        {
            return f(x);
        }
        catch (ArgumentException ex)
        {
            Console.WriteLine("MaMethodeQuiAppelleF");
            throw;
        }
        finally
        {
            Console.WriteLine("Bloc Finally");
        }
        
        Console.WriteLine("En dehors du try");
    }

}
}
