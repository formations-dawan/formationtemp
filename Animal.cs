﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonAppliConsole.Classes
{
    class Animal
    {
        static List<string> _races = new List<string> { "Chien", "Chat", "Poisson" };

        string _race;

        public string Nom { get; set; }

        public string Race
        {
            get
            {
                return _race;
            }
            set
            {
                /*if (!_races.Contains(value))
                    throw new KeyNotFoundException("Race invalide");*/

                _race = value;
            }
        }

        public Animal(string nom, string race)
        {
            //_race = race;
            Nom = nom;
            Race = race;
        }

        public virtual void Deplacer()
        {
            Console.WriteLine("Je me déplace.");
        }
    }
}
