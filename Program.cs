using System;

namespace MonAppliConsole
{
    class Program
    {
        struct MDouble
        {
            public int partieEntiereNb;
            public int partieDecimale;
        }

        public struct mDate
        {
            public int jour;
            public int mois;
            public int annee;
        }

        enum JoursSemaine
        {
            Lundi = 5,
            Mardi = 8,
            Mercredi,
            Jeudi,
            Vendredi,
            Samedi
        }

        enum Mois
        {
            Janvier,
            Fevrier,
            Mars
        }

        static void M1()
        {
            Console.WriteLine("Bonjour le monde Dawan !");
            Console.WriteLine("Salut Dawan");
            Console.WriteLine("Test");

            const string LUNDI = "Jour 1 de la semaine";
            const int MARDI = 2;
            const int FEVRIER = 3;

            var jourActuel = JoursSemaine.Mardi;
            Console.WriteLine(jourActuel);

            double d = 1.5; //1 partie entière + 1 partie décimale
            MDouble d2;
            d2.partieEntiereNb = 1;
            d2.partieDecimale = 5;

            mDate date;
            date.jour = 5;
            date.mois = 7;
            date.annee = 2022;

            mDate date2;
            date2.jour = 6;

            var jDate = 5;
            var mDate = 7;
            var aDate = 2022;
            var j2Date = 6;

            /* Opérateurs */
            var a = 5; // 0101
            var b = 10; // 1010

            var a1 = true;
            var a2 = false;

            Console.WriteLine(a + b);

            a++; //J'augmente a de 1
            a += 5; // a = a + 5 (1011)

            Console.WriteLine(a);

            /* && : ET ; || : OU ; ! : NON ; OU Exclusif : ^
             * a | b | && | || | ^ |        a | !
             * F | F | F  | F  | F |        F | V
             * F | V | F  | V  | V |        V | F
             * V | F | F  | V  | V |
             * V | V | V  | V  | F |
             */

            Console.WriteLine(true && false); // False
            Console.WriteLine(a1 || a2); // True
            Console.WriteLine(!(a1 ^ a2)); // False

            Console.WriteLine(a >= b); // False

            /* 1011 &
             * 1010 =
             * 1010 (10)
             */
            Console.WriteLine(a & b);

            /* Caractères d'échappement
             * \a Bell (alert)
             * \b retour en arrière
             * \n Nouvelle ligne
             * \r Retour chariot
             * \"
             * \\
             * \t
             */
            Console.WriteLine("\tSalut\nDawan");

            //Opérateur chaine verbatim @
            Console.WriteLine(@"Bonjour ""j""
                            Dawan");

            // Interpolation
            var nom = "Magalie";
            var lieu = "Dawan";
            Console.WriteLine("Bonjour {0}, tu es à {1}", nom, lieu);
            Console.WriteLine("Bonjour " + nom + ", tu es à " + lieu);

            Console.WriteLine($"Bonjour {nom}, tu es à {lieu}");

            // Transtypage
            var _double = 15.06;
            int _entier = (int)_double;

            double _double2 = _entier;

            var jour = Convert.ToInt32(Console.ReadLine());
            var jour2 = int.Parse(Console.ReadLine());


            // Conditions
            int test = 14;
            int test2 = 20;
            string res;
            if (test == 10)
            {
                res = "Bonjour";
            }
            else if (test == 14)
            {
                res = "Salut";
            }
            else
            {
                res = "Hello";
            }

            res = (test == 10) ? "Bonjour" : "Hello";

            Console.WriteLine(res);

            switch (test)
            {
                case 10:
                    res = "Bonjour";
                    break;
                case 14:
                    res = "Salut";
                    break;
                default:
                    res = "Hello";
                    break;
            }

            switch (jourActuel)
            {
                case JoursSemaine.Lundi:
                    break;
                case JoursSemaine.Mardi:
                    break;
                case JoursSemaine.Mercredi:
                    break;
                case JoursSemaine.Jeudi:
                    break;
                case JoursSemaine.Vendredi:
                    break;
                case JoursSemaine.Samedi:
                    break;
                default:
                    break;
            }

            if (jourActuel == JoursSemaine.Lundi)
            {

            }
            else if (jourActuel == JoursSemaine.Mardi)
            {

            }

            int salutations = 1;

            switch (salutations)
            {
                case 1:
                    Console.WriteLine("Hello 2");
                    goto default;
                case 2:
                    Console.WriteLine("Bonjour 2");
                    goto case 3;
                case 3:
                    Console.WriteLine("Hallo 2");
                    goto default;
                default:
                    Console.WriteLine("Valeur donnée est :" + salutations);
                    break;
            }

            // Quand on connait le nombre d'itérations à l'avance
            for (int i = 1; i <= 5; i += 2)
            {
                Console.WriteLine("Bonjour");
                Console.WriteLine(i);
            }


            Console.Write(@"Rentrez la phrase ""salut"" : ");
            string saisie = Console.ReadLine();
            // Quand on ne connait pas le nombre d'itérations à l'avance (on est pas obligé de passer dans la boucle)
            while (saisie != "salut")
            {
                Console.Write(@"Rentrez la phrase ""salut"" : ");
                saisie = Console.ReadLine();
            }
            //string saisie;
            // Quand on ne connait pas le nombre d'itérations à l'avance (on passe au moins 1 fois)
            do
            {
                Console.Write(@"Rentrez la phrase ""salut"" : ");
                saisie = Console.ReadLine();
            } while (saisie != "salut");



            int _a = 10, _b = 23;
            var _res = 0;

            for (int i = 1; i <= _b; i++)
            {
                _res += _a;
            }

            Console.WriteLine(_res);
        }

        static void M2()
        {
            int[] tab1 = new int[] { 1, 2, 3, 15, 78, 80, 12 };

            int var1 = tab1[5];

            Console.WriteLine(var1);

            for (int i = 0; i < tab1.Length; i++)
            {
                var nombre = tab1[i];

                Console.WriteLine(nombre);
            }

            foreach (var nombre in tab1) // nombre = tab[i]
            {
                Console.WriteLine(nombre);
            }

            int var2 = 5;
            int var3 = var2; // Copie de var2 dans var3
            int[] tab2; //null

            var tabD = new int[3];

            int[] tabD2 = tabD; // tabD2 contient la même adresse que tabD
            tabD = null; // Garbage Collector (ramasse-miette)
            tabD = tabD2;

            string[,,] tab3d = { { { "DIARTE", "Magalie" }, { "Marseille", "13000" } }, { { "HENOT", "Florian" }, { "Azerables", "23160" } } };
            tab3d[0, 0, 0] = "DIARTE";
            tab3d[0, 0, 1] = "Magalie";
            tab3d[0, 1, 0] = "Marseille";
            tab3d[0, 1, 1] = "13000";
            tab3d[1, 0, 0] = "HENOT";
            tab3d[1, 0, 1] = "Florian";
            tab3d[1, 1, 0] = "Azerables";
            tab3d[1, 1, 1] = "23160";

            int[][] tab4 = new int[3][];
            tab4[0] = new int[] { 45, 62, 32 };
            tab4[1] = new int[6];


            Console.WriteLine(tab4[1][5]);

            foreach (var item in tab3d)
            {
                Console.WriteLine(item);
            }


            // Algorithme
            int x1 = 200, y1 = 4;
            int compteur = 0;
            string[,] tabA = new string[x1, y1];


            for (int i1 = 0; i1 < x1; i1++)
                for (int j1 = 0; j1 < y1; j1++)
                    tabA[i1, j1] = (++compteur).ToString(); // ++ devant la variable =  var a = b = b + 1 ; ++ après la variable = var a = b; b = b + 1

            Console.Write("Entrer le nombre à chercher : ");

            var nb = Console.ReadLine();
            var trouve = false;

            for (int i1 = 0; i1 < x1; i1++)
            {
                for (int j1 = 0; j1 < y1; j1++)
                {
                    if (tabA[i1, j1].Equals(nb))
                    {
                        Console.WriteLine($"Le nombre {nb} a été trouvé.");
                        trouve = true;
                        break;
                    }
                }

                if (trouve) break;
            }

            if (!trouve)
                Console.WriteLine($"Le nombre {nb} n'a pas été trouvé.");


            if (trouve) // trouve == true car trouve est un booléen et if fonctionne avec des booléens
            {
                Console.WriteLine($"Le nombre {nb} a été trouvé.");
            }
            else
            {
                Console.WriteLine($"Le nombre {nb} n'a pas été trouvé.");
            }
            /*Console.WriteLine($"Le nombre {nb} n'a pas été trouvé.");
            goto Fin;

            Trouve:
            Console.WriteLine($"Le nombre {nb} a été trouvé.");

            Fin:
            Console.WriteLine("Fin de la recherche.");*/
        }

        static void M3()
        {
            int somme;
            int min;
            somme = MethodesCalcul.Somme(5, 10, out min);
            Console.WriteLine(somme);

            //int somme2 = 20;
            MethodesCalcul.Somme2(5, 10, out int somme2);
            Console.WriteLine(somme2);

            int entree = 15;
            MethodesCalcul.Transform(ref entree);
            Console.WriteLine(entree);

            string nom = MethodesCalcul.GetNom();
            var y = MethodesCalcul.f(5);

            mDate date1;
            date1.jour = 5;
            date1.mois = 12;
            date1.annee = 2022;

            mDate date2;
            date2.jour = 10;
            date2.mois = 1;
            date2.annee = 2019;

            mDate date3;
            date3.jour = 25;
            date3.mois = 07;
            date3.annee = 2005;

            MethodesCalcul.Enregistrer(out int nb, date1, date2, date3);
            Console.WriteLine(nb);

            var somme3 = MethodesCalcul.Somme3(5, 10, 0, e: 1);

            var somme4 = MethodesCalcul.Somme4("5", "10");
            Console.WriteLine(somme4);
        }

        static void M4()
        {
            try
            {
                var res = MethodesCalcul.MaMethodeQuiAppelleF(2);
            }
            catch (ApplicationException ex)
            {
                Console.WriteLine("Une erreur s'est produte dans l'exécution de la méthode.");
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Une erreur s'est produte à cause de l'argument d'une méthode.");
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Une erreur inconnue s'est produite.");
                Console.WriteLine(ex.Message);
            }
        }

        static void Main(string[] args)
        {
            var mc = new MethodesCalcul();
            mc.InfosObjet();

            var animal1 = new Classes.Animal("Noisette", "Golden Retriever");
            animal1.Nom = "Jedy";
            Console.WriteLine(animal1.Race);
            animal1.Deplacer();

            var chien1 = new Classes.Chien("Jedy", "Golden Retriever", "Noir");
            Console.WriteLine(chien1.CouleurPelage);
            chien1.Deplacer();

            var chat1 = new Classes.Chat("Jedy", "Golden Retriever", "Noir");
            Console.WriteLine(chat1.CouleurPelage);
            chat1.Deplacer();

            var poisson1 = new Classes.Poisson("Margeurite", "Combattant", 3);
            Console.WriteLine(poisson1.NbNageoires);
            poisson1.Deplacer();
        }
    }
}
