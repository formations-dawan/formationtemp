﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonAppliConsole.Classes
{
    class Chien : Animal
    {
        public string CouleurPelage { get; set; }

        public Chien(string nom, string race) : base(nom, race)
        {
            CouleurPelage = "Sable";
        }

        public Chien(string nom, string race, string couleurPelage) : base(nom, race)
        {
            CouleurPelage = couleurPelage;
        }

        public override void Deplacer()
        {
            base.Deplacer();
            Console.WriteLine($"Le chien {Nom} marche");
        }
    }
}
