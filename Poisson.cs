﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonAppliConsole.Classes
{
    class Poisson : Animal
    {
        public int NbNageoires { get; set; }

        public Poisson(string nom, string race, int nbNageoires) : base(nom, race)
        {
            NbNageoires = nbNageoires;
        }

        public override void Deplacer()
        {
            Console.WriteLine($"Le poisson {Nom} nage");
        }
    }
}
