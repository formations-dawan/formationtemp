﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonAppliConsole.Classes
{
    class Chat : Chien
    {
        public Chat(string nom, string race) : base(nom, race, "Noir")
        { }

        public Chat(string nom, string race, string couleurPelage) : base(nom, race, couleurPelage)
        { }

        public override void Deplacer()
        {
            Console.WriteLine($"Le chat {Nom} marche");
        }
    }
}
